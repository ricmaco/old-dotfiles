function fish_prompt
  set -l last_command_status $status

  set -l lavender (set_color -o dfafff)
  set -l pink (set_color -o ffafdf)
  set -l mint (set_color -o afffd7)
  set -l light_blue (set_color -o afffff)
  set -l light_purple (set_color -o ffdfff)
  set -l dark_green (set_color -o 00875f)
  set -l dark_pink (set_color -o ff005f)
  set -l dark_blue (set_color -o 0087af)
  set -l normal (set_color normal)
  
  if not set -q -g __fish_robbyrussell_functions_defined
    set -g __fish_robbyrussell_functions_defined
  
    function _git_prompt
      set -l hex_pink "ffafdf"
      set -l hex_light_purple "ffdfff"
      set -l hex_light_blue "afffff"

      set -gx __fish_git_prompt_showdirtystate 1
      set -gx __fish_git_prompt_showstashstate 1
      set -gx __fish_git_prompt_showuntrackedfiles 1
      set -gx __fish_git_prompt_showupstream 'auto'
      set -gx __fish_git_prompt_show_informative_status 1
      set -gx __fish_git_prompt_color "-o" "$hex_pink"
      set -gx __fish_git_prompt_color_bare "-o" "$hex_light_blue"
      set -gx __fish_git_prompt_color_merging "-o" "$hex_light_blue"
      set -gx __fish_git_prompt_color_branch "-o" "$hex_light_purple"
      set -gx __fish_git_prompt_color_flags "-o" "$hex_light_blue"
      set -gx __fish_git_prompt_color_upstream "-o" "$hex_light_blue"
      set -gx __fish_git_prompt_color_cleanstate "-o" "$hex_light_blue"
      set -gx __fish_git_prompt_color_flags "-o" "$hex_light_blue"
      __fish_git_prompt "{%s}"
    end

  end

  set -l symbol_color "$dark_blue"
  if test $last_command_status != 0
    set symbol_color "$lavender"
  end

  set -l symbol "$symbol_color%"
  if test (id -u) = 0
    set symbol "$symbol_color#"
  end

  set -l cwd $mint(basename (prompt_pwd))

  set -l git_prompt (_git_prompt)
  if [ $git_prompt ]
    set git "$pink git:$git_prompt"
  end

  set -l venv_name (basename "$VIRTUAL_ENV")
  if set -q VIRTUAL_ENV
    set venv "$pink venv:{$light_purple$venv_name$pink}"
  end

  echo -n -s $symbol' '$cwd $git $venv $normal' '
end
