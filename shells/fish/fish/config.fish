# source virtualfish
eval (python -m virtualfish compat_aliases auto_activation)

# alias
alias c 'cd'
alias ls 'ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias l 'ls'
alias ll 'ls -lh --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la 'ls -lah --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias tree 'tree -Csuh'
alias grep 'grep --color=tty -d skip'
alias cp 'cp -i'
alias rm 'rm -Iv'
alias df 'df -h'
alias free 'free -m'
alias du 'du -shm'
alias df 'df -kTh'
alias diff 'diff -u --color'


# conditional alias
if type -q youtube-dl
  alias yta 'youtube-dl -w -x -i --audio-format vorbis'
  alias ytw 'youtube-dl -w -x -i --audio-format wav'
  alias ytm 'youtube-dl -w -x -i --audio-format mp3'
  alias ytv 'youtube-dl -w -i -f best'
end

function man
  set -g -x LESS_TERMCAP_mb (printf '\e[01;31m')
  set -g -x LESS_TERMCAP_md (printf '\e[01;38;5;74m')
  set -g -x LESS_TERMCAP_me (printf '\e[0m')
  set -g -x LESS_TERMCAP_se (printf '\e[0m')
  set -g -x LESS_TERMCAP_so (printf '\e[38;5;246m')
  set -g -x LESS_TERMCAP_ue (printf '\e[0m')
  set -g -x LESS_TERMCAP_us (printf '\e[04;38;5;146m')
  env man $argv
end

function cd
  builtin cd $argv
  ls
end

# motd
function isroot
  return (test (id -u) -eq 0)
end
function fish_greeting
  if not isroot
    if type -q neofetch
      neofetch
    end
  end
end
