#!/usr/bin/env bash
set -e

echo "Mounting snapshot volume..."
/usr/bin/mkdir -p /mnt/snapshot
/usr/bin/mount -t ext4 -o rw,defaults,noatime -L snapshot /mnt/snapshot
echo "Done"
echo "Starting rsync copy from data to snapshot..."
/usr/bin/rsync -av --delete /mnt/data/ /mnt/snapshot
echo "Done"
echo "Unmounting snapshot volume..."
/usr/bin/umount /mnt/snapshot
/usr/bin/rmdir /mnt/snapshot
echo "Done"
