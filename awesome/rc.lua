-- Config works with:
-- - volumeicon
-- - wicd-gtk
-- - dropbox
-- - vicious
-- - urxvt
-- - vim
-- - pcmanfm
-- - opera
-- - thunderbird
-- - i3lock
-- - xautolock
-- - nitrogen
-- - gsimplecal
-- - xpad
-- - gksu
-- - autocutsel

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
local vicious = require("vicious")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Configuration directory
fs_conf = awful.util.getdir("config")

-- Themes define colours, icons, font and wallpapers.
beautiful.init(fs_conf .. "/themes/steamlined/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvtc"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor
browser = "opera"
file_manager = "pcmanfm"
mail_client = "thunderbird"
lockscreen = "xautolock -locknow"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
--    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
--    awful.layout.suit.fair,
--    awful.layout.suit.fair.horizontal,
--    awful.layout.suit.spiral,
--    awful.layout.suit.spiral.dwindle,
--    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
--    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ "Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ", "Ⅵ", "Ⅶ", "Ⅷ", "Ⅸ" }, s, layouts[1])
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
mymainmenu = awful.menu({ items = { { "browser", browser }, 
                                    { "terminal", terminal },
                                    { "vim", editor_cmd },
                                    { "files", file_manager },
                                    { "email", mail_client },
                                    { "editor", "mousepad" },
                                    { "libreoffice", "libreoffice" },
                                    { "gimp", "gimp" },
                                    { "gummi", "gummi" },
                                    { "keepass", "keepassx" },
                                    { "gparted", "gksu gparted" },
                                    { "wallpaper", "nitrogen" }
                                  }
                        })

mylaunchermenu = awful.menu({ items = { { "config", editor_cmd .. " " .. awesome.conffile },
                                        { "rehash", awesome.restart },
                                        { "lock", lockscreen },
                                        { "logout", awesome.quit },
                                        { "suspend", "systemctl suspend" },
                                        { "hibernate", "systemctl hibernate" },
                                        { "restart", "systemctl reboot" },   
                                        { "shutdown", "systemctl poweroff" }                                       
                                      }
                        })

mylauncher = awful.widget.launcher({ image = fs_conf .. "/icon.png",
                                     menu = mylaunchermenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox

-- Create a separator widget
spacer = wibox.widget.textbox()
spacer:set_markup("<span>  </span>")

-- Create a battery widget
baticon = wibox.widget.imagebox()
baticon:set_resize(false)
baticon:set_image(fs_conf .. "/battery.png")

batwidget = wibox.widget.textbox()
vicious.register(batwidget, vicious.widgets.bat, "<span><b>$1$2%</b></span>", 32, "BAT0")

-- Create a textclock widget
clockwidget = wibox.widget.textbox()
clockwidget:buttons(awful.util.table.join(
  awful.button({ }, 1, function () awful.util.spawn("gsimplecal") end)
))
vicious.register(clockwidget, vicious.widgets.date, "<span> <b>%a, %d %B · %H:%M</b></span>", 60)

-- Create a memory widget
memicon = wibox.widget.imagebox()
memicon:set_resize(false)
memicon:set_image(fs_conf .. "/mem.png")


memwidget = wibox.widget.textbox()
vicious.register(memwidget, vicious.widgets.mem, "<span><b>$2 MB</b></span>", 13)

-- Create a CPU widget
cpuicon = wibox.widget.imagebox()
cpuicon:set_resize(false)
cpuicon:set_image(fs_conf .. "/cpu.png")


cpuwidget = wibox.widget.textbox()
vicious.register(cpuwidget, vicious.widgets.cpu, "<span><b>$1%</b></span>", 3)

-- Create a textclock widget
caffeinestatus = true
caffeinewidget = wibox.widget.imagebox()
caffeinewidget:set_resize(false)
caffeinewidget:set_image(fs_conf .. "/caffeine.png")
caffeinewidget:buttons(awful.util.table.join(
  awful.button({ }, 1, function () 
                          if os.execute("pgrep xautolock") then
                            if caffeinestatus then
                              awful.util.spawn("xautolock -disable")
                              awful.util.spawn("xset dpms force on")
                              caffeinestatus = false
                              naughty.notify({ preset = naughty.config.presets.normal,
                                               title = "Caffeine",
                                               text = "Screensaver disabled." })
                            else
                              awful.util.spawn("xautolock -enable")
                              caffeinestatus = true
                              naughty.notify({ preset = naughty.config.presets.normal,
                                               title = "Caffeine",
                                               text = "Screensaver enabled." })
                            end
                          else
                            caffeinestatus = false
                            naughty.notify({ preset = naughty.config.presets.normal,
                                             title = "Caffeine",
                                             text = "Screensaver not in execution." })
                          end
                        end)
))


-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({
                                                      theme = { width = 250 }
                                                  })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(spacer)
    left_layout:add(mylauncher)
    left_layout:add(spacer)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then
        right_layout:add(spacer)
        right_layout:add(spacer)
        right_layout:add(wibox.widget.systray())
        right_layout:add(caffeinewidget)
        right_layout:add(spacer)
    else
        right_layout:add(spacer)
    end
    right_layout:add(spacer)
    right_layout:add(baticon)
    right_layout:add(batwidget)
    
    right_layout:add(spacer)
    right_layout:add(cpuicon)
    right_layout:add(cpuwidget)

    right_layout:add(spacer)
    right_layout:add(memicon)
    right_layout:add(memwidget)

    right_layout:add(spacer)
    right_layout:add(clockwidget)
    
    right_layout:add(spacer)
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),
    
    awful.key({ modkey, "Control" }, "n", awful.client.restore),
    
    -- Custom keys
    awful.key({ modkey, "Shift" }, "f", function () awful.util.spawn(browser)   end),
    awful.key({ modkey, "Shift" }, "s", function () awful.util.spawn(mail_client)  end),
    awful.key({ modkey, "Shift" }, "t", function () awful.util.spawn(file_manager)   end),
    awful.key({ "Control", "Mod1" }, "s", function() awful.util.spawn("systemctl poweroff")   end),
    awful.key({ "Control", "Mod1" }, "r", function() awful.util.spawn("systemctl reboot")   end),
    awful.key({ "Control", "Mod1" }, "h", function() awful.util.spawn("systemctl hibernate")   end),
    awful.key({ "Control", "Mod1" }, "l", function() awful.util.spawn(lockscreen)   end),

    -- Resize floating windows
    awful.key({ modkey }, "Next",
              function ()
                  if client.focus then
                      awful.client.moveresize( 0, 0, 0, 10)
                  end
              end),
    awful.key({ modkey }, "Prior",
              function ()
                  if client.focus then
                      awful.client.moveresize( 0, 0, 0, -10)
                  end
              end),
    awful.key({ modkey }, "Home",
              function ()
                  if client.focus then
                      awful.client.moveresize( 0, 0, 10, 0)
                  end
              end),
    awful.key({ modkey }, "End",
              function ()
                  if client.focus then
                      awful.client.moveresize( 0, 0, -10, 0)
                  end
              end),

    -- Prompt
    awful.key({ modkey }, "r", function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),

    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end),
    
    -- Toggle all tags
    awful.key({ modkey, "Control", "Shift" }, "a",
              function ()
                  if client.focus then
                      local tags = awful.tag.gettags(client.focus.screen)
                      if tags then
                          for i = 1, #tags do
                              awful.client.toggletag(tags[i])
                          end
                      end
                  end
              end)
)

-- Bind dual screen keys only if there are more than one screen.
if screen.count() > 1 then
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey, "Control" }, "o",     function () awful.screen.focus(2)         end),
        awful.key({ modkey, "Control" }, "p",     function () awful.screen.focus(1)         end))
end

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end),
        -- Toggle tag.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.toggletag(tag)
                          end
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     size_hints_honor = false },
      callback = awful.client.setasslave
    },
    { rule_any = { class = { "Skype", "plugin-container", "exe", "xpad" } },
      properties = { floating = true } },
    { rule_any = { class = { "gsimplecal", "Gsimplecal" } },
      callback = function(c)
          awful.placement.under_mouse(c)
          awful.placement.no_offscreen(c)
      end }
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- {{{ Autostart apps
function run_once(name, command)
        awful.util.spawn_with_shell(
                "ps aux | grep " .. name .. " | grep -v grep >/dev/null 2>&1 || " ..
                      command .. " >/dev/null 2>&1"
        )
end

run_once("autocutsel -fork", "autocutsel -fork")
run_once("autocutsel -selection PRIMARY -fork", "autocutsel -selection PRIMARY -fork")
run_once("urxvtd", "urxvtd")
run_once("nitrogen", "nitrogen --restore")
run_once("volumeicon", "volumeicon")
run_once("wicd-client", "wicd-client --tray")
run_once("xautolock", "xautolock -time 10 -locker 'i3lock -t -i .lockscreen' -detectsleep")
run_once("dropbox", "dropbox")
run_once("xpad", "xpad --hide")

-- }}}
