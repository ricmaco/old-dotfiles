" vim-plug
call plug#begin()

" Interface
Plug 'itchyny/lightline.vim'
Plug 'flazz/vim-colorschemes'
Plug 'Yggdroot/indentLine'

" General programming
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-commentary'
Plug 'ervandew/supertab'

call plug#end()

" Encoding in UTF-8
set encoding=utf-8

" keep separated, distro-specific undo history.
set hidden

" Do not show mode
set noshowmode

" Case insensitive search, except for capital.
set ignorecase
set smartcase

" Backspacing over autoindent, line breaks, start of insert.
set backspace=indent,eol,start

" Set a text width of 80 characters
set textwidth=80
set cc=80

" Raise a dialogue asking to save changes instead of fail a command.
set confirm

" Use a visual bell, instead of a sound one.
set visualbell
set t_vb=

" Enable mouse use, for compatibility.
set mouse=a

" Display line numbers on the left.
set number

" Default indentation at 2 spaces.
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Lightline
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ }

" Indent line with plugin
let g:indentLine_setConceal=2
let g:indentLine_color_term=59
let g:indentLine_leadingSpaceEnabled=1
let g:indentLine_char='│'
let g:indentLine_leadingSpaceChar='·'

" Don't write a backup .ext~ file
set nobackup

" Set <Leader> to coma.
let mapleader=","

" Fix for transparent background
set background="dark"
autocmd colorscheme *
      \ highlight Normal ctermbg=none |
      \ highlight NonText ctermbg=none |
      \ highlight Statement ctermbg=none |
      \ highlight Title ctermbg=none |
      \ highlight Todo ctermbg=none |
      \ highlight Underlined ctermbg=none |
      \ highlight ErrorMsg ctermbg=none |
      \ highlight LineNr ctermbg=none

" Set colorscheme
colorscheme 1989

" Split options
set splitbelow
set splitright

" Move between splits easily
nnoremap <C-Del> <C-W><Down>
nnoremap <C-Insert> <C-W><Up>
nnoremap <C-PageDown> <C-W><Right>
nnoremap <C-PageUp> <C-W><Left>

" Use system PRIMARY clipboard
set clipboard=unnamed
